import Listener from "./listener.mjs";
import Person from "./person.mjs";

class Speaker extends Person {
    say(phrase) {

        console.log(`"${super.say(phrase)}" very confidently`)

    }
}

let john = new Person('John');
console.log(john.say('Hello!'));

let bob = new Speaker('Bob');
console.log(bob.say('Hi!'));
 
let carl = new Listener('Carl');
console.log(carl.say('Hello and Hi'));
