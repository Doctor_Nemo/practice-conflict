export default class Listener {
    constructor(name) {
        this.name = name;
    }

    say(phrase) {
        return `${this.name} hear ${phrase}`;
    }
}

